package com.santa.demosupermovil;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class AppsUtil {
    private ArrayList<App> apps = new ArrayList<App>();

    public ArrayList<App> getApps() {
        return apps;
    }

    public void setApps(ArrayList<App> apps) {
        this.apps = apps;
    }

    @Override
    public String toString() {
        String appValues="AppsUtil{apps=\n";
        for (App app:apps) {
            appValues = appValues + app.toString() + "\n";
        }
        appValues = appValues + "}";

        return appValues;
    }

    public CharSequence[] getAppNamesCharSequence() {
        List<String> items = new ArrayList<String>();

        for (App app:apps) {
            /*Log.d("RRS", app.getName());*/
            items.add(app.getName());
        }

        return items.toArray(new CharSequence[items.size()]);
    }

    public boolean[] getAppAddedStatus() {
        boolean[] items = new boolean[apps.size()];

        for (int i = 0; i < apps.size(); i++) {
            items[i] = apps.get(i).isAdded();
        }

        return items;
    }
}
