package com.santa.demosupermovil;

import android.app.Activity;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Dashboard extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    public static final String KEY_LINK = "link";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    protected void onResume() {
        super.onResume();
        manageApps();
    }

    private void manageApps() {
        AppsUtil appsUtil = getApps();
        Log.d("RRS - manageApps", appsUtil.toString());

        cleanAppsPanel();

        for (int i = 2; i < appsUtil.getApps().size() + 2; i++) {
            switch (i) {
                case 2:
                    LinearLayout linearLayoutb1 = (LinearLayout) findViewById(R.id.b1);
                    linearLayoutb1.setVisibility(View.VISIBLE);
                    ImageButton imageButtonb1 = (ImageButton) findViewById(R.id.btn_b1);
                    imageButtonb1.setVisibility(View.VISIBLE);
                    TextView textViewb1 = (TextView) findViewById(R.id.lbl_b1);
                    textViewb1.setVisibility(View.VISIBLE);
                    textViewb1.setText(appsUtil.getApps().get(i - 2).getName());
                    int imageb1 = appsUtil.getApps().get(i - 2).isEnabled() ? R.drawable.santander_plus : R.drawable.plus_icon;
                    imageButtonb1.setImageResource(imageb1);
                    break;
                case 3:
                    LinearLayout linearLayoutc1 = (LinearLayout) findViewById(R.id.c1);
                    linearLayoutc1.setVisibility(View.VISIBLE);
                    ImageButton imageButtonc1 = (ImageButton) findViewById(R.id.btn_c1);
                    imageButtonc1.setVisibility(View.VISIBLE);
                    TextView textViewc1 = (TextView) findViewById(R.id.lbl_c1);
                    textViewc1.setVisibility(View.VISIBLE);
                    textViewc1.setText(appsUtil.getApps().get(i - 2).getName());
                    int imagec1 = appsUtil.getApps().get(i - 2).isEnabled() ? R.drawable.santander_plus : R.drawable.plus_icon;
                    imageButtonc1.setImageResource(imagec1);
                    break;
                case 4:
                    TableRow tableRow2 = (TableRow) findViewById(R.id.row2);
                    tableRow2.setVisibility(View.VISIBLE);

                    LinearLayout linearLayouta2 = (LinearLayout) findViewById(R.id.a2);
                    linearLayouta2.setVisibility(View.VISIBLE);
                    ImageButton imageButtona2 = (ImageButton) findViewById(R.id.btn_a2);
                    imageButtona2.setVisibility(View.VISIBLE);
                    TextView textViewa2 = (TextView) findViewById(R.id.lbl_a2);
                    textViewa2.setVisibility(View.VISIBLE);
                    textViewa2.setText(appsUtil.getApps().get(i - 2).getName());
                    int imagea2 = appsUtil.getApps().get(i - 2).isEnabled() ? R.drawable.santander_plus : R.drawable.plus_icon;
                    imageButtona2.setImageResource(imagea2);
                    break;
                case 5:
                    LinearLayout linearLayoutb2 = (LinearLayout) findViewById(R.id.b2);
                    linearLayoutb2.setVisibility(View.VISIBLE);
                    ImageButton imageButtonb2 = (ImageButton) findViewById(R.id.btn_b2);
                    imageButtonb2.setVisibility(View.VISIBLE);
                    TextView textViewb2 = (TextView) findViewById(R.id.lbl_b2);
                    textViewb2.setVisibility(View.VISIBLE);
                    textViewb2.setText(appsUtil.getApps().get(i - 2).getName());
                    int imageb2 = appsUtil.getApps().get(i - 2).isEnabled() ? R.drawable.santander_plus : R.drawable.plus_icon;
                    imageButtonb2.setImageResource(imageb2);
                    break;
                case 6:
                    LinearLayout linearLayoutc2 = (LinearLayout) findViewById(R.id.c2);
                    linearLayoutc2.setVisibility(View.VISIBLE);
                    ImageButton imageButtonc2 = (ImageButton) findViewById(R.id.btn_c2);
                    imageButtonc2.setVisibility(View.VISIBLE);
                    TextView textViewc2 = (TextView) findViewById(R.id.lbl_c2);
                    textViewc2.setVisibility(View.VISIBLE);
                    textViewc2.setText(appsUtil.getApps().get(i - 2).getName());
                    int imagec2 = appsUtil.getApps().get(i - 2).isEnabled() ? R.drawable.santander_plus : R.drawable.plus_icon;
                    imageButtonc2.setImageResource(imagec2);
                    break;
                case 7:
                    TableRow tableRow3 = (TableRow) findViewById(R.id.row3);
                    tableRow3.setVisibility(View.VISIBLE);

                    LinearLayout linearLayouta3 = (LinearLayout) findViewById(R.id.a3);
                    linearLayouta3.setVisibility(View.VISIBLE);
                    ImageButton imageButtona3 = (ImageButton) findViewById(R.id.btn_a3);
                    imageButtona3.setVisibility(View.VISIBLE);
                    TextView textViewa3 = (TextView) findViewById(R.id.lbl_a3);
                    textViewa3.setVisibility(View.VISIBLE);
                    textViewa3.setText(appsUtil.getApps().get(i - 2).getName());
                    int imagea3 = appsUtil.getApps().get(i - 2).isEnabled() ? R.drawable.santander_plus : R.drawable.plus_icon;
                    imageButtona3.setImageResource(imagea3);
                    break;
                case 8:
                    LinearLayout linearLayoutb3 = (LinearLayout) findViewById(R.id.b3);
                    linearLayoutb3.setVisibility(View.VISIBLE);
                    ImageButton imageButtonb3 = (ImageButton) findViewById(R.id.btn_b3);
                    imageButtonb3.setVisibility(View.VISIBLE);
                    TextView textViewb3 = (TextView) findViewById(R.id.lbl_b3);
                    textViewb3.setVisibility(View.VISIBLE);
                    textViewb3.setText(appsUtil.getApps().get(i - 2).getName());
                    int imageb3 = appsUtil.getApps().get(i - 2).isEnabled() ? R.drawable.santander_plus : R.drawable.plus_icon;
                    imageButtonb3.setImageResource(imageb3);
                    break;
                case 9:
                    LinearLayout linearLayoutc3 = (LinearLayout) findViewById(R.id.c3);
                    linearLayoutc3.setVisibility(View.VISIBLE);
                    ImageButton imageButtonc3 = (ImageButton) findViewById(R.id.btn_c3);
                    imageButtonc3.setVisibility(View.VISIBLE);
                    TextView textViewc3 = (TextView) findViewById(R.id.lbl_c3);
                    textViewc3.setVisibility(View.VISIBLE);
                    textViewc3.setText(appsUtil.getApps().get(i - 2).getName());
                    int imagec3 = appsUtil.getApps().get(i - 2).isEnabled() ? R.drawable.santander_plus : R.drawable.plus_icon;
                    imageButtonc3.setImageResource(imagec3);
                    break;
                case 10:
                    TableRow tableRow4 = (TableRow) findViewById(R.id.row4);
                    tableRow4.setVisibility(View.VISIBLE);

                    LinearLayout linearLayouta4 = (LinearLayout) findViewById(R.id.a4);
                    linearLayouta4.setVisibility(View.VISIBLE);
                    ImageButton imageButtona4 = (ImageButton) findViewById(R.id.btn_a4);
                    imageButtona4.setVisibility(View.VISIBLE);
                    TextView textViewa4 = (TextView) findViewById(R.id.lbl_a4);
                    textViewa4.setVisibility(View.VISIBLE);
                    textViewa4.setText(appsUtil.getApps().get(i - 2).getName());
                    int imagea4 = appsUtil.getApps().get(i - 2).isEnabled() ? R.drawable.santander_plus : R.drawable.plus_icon;
                    imageButtona4.setImageResource(imagea4);
                    break;
            }
        }
    }

    private void cleanAppsPanel() {
        TableRow tableRow2 = (TableRow) findViewById(R.id.row2);
        tableRow2.setVisibility(View.GONE);
        TableRow tableRow3 = (TableRow) findViewById(R.id.row3);
        tableRow3.setVisibility(View.GONE);
        TableRow tableRow4 = (TableRow) findViewById(R.id.row4);
        tableRow4.setVisibility(View.GONE);

        LinearLayout linearLayoutb1 = (LinearLayout) findViewById(R.id.b1);
        linearLayoutb1.setVisibility(View.INVISIBLE);
        LinearLayout linearLayoutc1 = (LinearLayout) findViewById(R.id.c1);
        linearLayoutc1.setVisibility(View.INVISIBLE);

        LinearLayout linearLayouta2 = (LinearLayout) findViewById(R.id.a2);
        linearLayouta2.setVisibility(View.INVISIBLE);
        LinearLayout linearLayoutb2 = (LinearLayout) findViewById(R.id.b2);
        linearLayoutb2.setVisibility(View.INVISIBLE);
        LinearLayout linearLayoutc2 = (LinearLayout) findViewById(R.id.c2);
        linearLayoutc2.setVisibility(View.INVISIBLE);
        LinearLayout linearLayouta3 = (LinearLayout) findViewById(R.id.a3);
        linearLayouta3.setVisibility(View.INVISIBLE);
        LinearLayout linearLayoutb3 = (LinearLayout) findViewById(R.id.b3);
        linearLayoutb3.setVisibility(View.INVISIBLE);
        LinearLayout linearLayoutc3 = (LinearLayout) findViewById(R.id.c3);
        linearLayoutc3.setVisibility(View.INVISIBLE);
        LinearLayout linearLayouta4 = (LinearLayout) findViewById(R.id.a4);
        linearLayouta4.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                /*alertaSalir();*/
                break;
            case 4:
                mTitle = getString(R.string.title_section4);
                alertaSalir();
                break;
        }
        /*manageApps();*/
    }

    private void logout() {
        finish();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        logout();
        super.onDestroy();
    }

    public void onApps(View view) {
        alertaApps();
    }

    public void onSaldos(View view) {
        Toast.makeText(this, R.string.str_saldos, Toast.LENGTH_SHORT).show();
    }

    public void onPagos(View view) {
        Toast.makeText(this, R.string.str_pagos, Toast.LENGTH_SHORT).show();
    }

    public void onTransfers(View view) {
        Toast.makeText(this, R.string.str_transfers, Toast.LENGTH_SHORT).show();
    }

    public void onSupertoken(View view) {
        Toast.makeText(this, R.string.str_supertoken, Toast.LENGTH_SHORT).show();
    }

    public void onGenericApp(View view) {

        App app;
        switch (view.getId()) {
            case R.id.btn_b1:
                TextView textViewb1 = (TextView) findViewById(R.id.lbl_b1);
                app = getApp(textViewb1.getText().toString());
                if (!app.isEnabled()) {
                    alertaUnabled(app);
                } else {
                    resolveActivity(app);
                }
                break;
            case R.id.btn_c1:
                TextView textViewc1 = (TextView) findViewById(R.id.lbl_c1);
                app = getApp(textViewc1.getText().toString());
                if (!app.isEnabled()) {
                    alertaUnabled(app);
                } else {
                    resolveActivity(app);
                }
                break;
            case R.id.btn_a2:
                TextView textViewa2 = (TextView) findViewById(R.id.lbl_a2);
                app = getApp(textViewa2.getText().toString());
                if (!app.isEnabled()) {
                    alertaUnabled(app);
                } else {
                    resolveActivity(app);
                }
                break;
            case R.id.btn_b2:
                TextView textViewb2 = (TextView) findViewById(R.id.lbl_b2);
                app = getApp(textViewb2.getText().toString());
                if (!app.isEnabled()) {
                    alertaUnabled(app);
                } else {
                    resolveActivity(app);
                }
                break;
            case R.id.btn_c2:
                TextView textViewc2 = (TextView) findViewById(R.id.lbl_c2);
                app = getApp(textViewc2.getText().toString());
                if (!app.isEnabled()) {
                    alertaUnabled(app);
                } else {
                    resolveActivity(app);
                }
                break;
            case R.id.btn_a3:
                TextView textViewa3 = (TextView) findViewById(R.id.lbl_a3);
                app = getApp(textViewa3.getText().toString());
                if (!app.isEnabled()) {
                    alertaUnabled(app);
                } else {
                    resolveActivity(app);
                }
                break;
            case R.id.btn_b3:
                TextView textViewb3 = (TextView) findViewById(R.id.lbl_b3);
                app = getApp(textViewb3.getText().toString());
                if (!app.isEnabled()) {
                    alertaUnabled(app);
                } else {
                    resolveActivity(app);
                }
                break;
            case R.id.btn_c3:
                TextView textViewc3 = (TextView) findViewById(R.id.lbl_c3);
                app = getApp(textViewc3.getText().toString());
                if (!app.isEnabled()) {
                    alertaUnabled(app);
                } else {
                    resolveActivity(app);
                }
                break;
            case R.id.btn_a4:
                TextView textViewa4 = (TextView) findViewById(R.id.lbl_a4);
                app = getApp(textViewa4.getText().toString());
                if (!app.isEnabled()) {
                    alertaUnabled(app);
                } else {
                    resolveActivity(app);
                }
                break;
        }
    }

    private void resolveActivity(App app) {
        Intent i = new Intent();
        Bundle bundle = new Bundle();

        switch (app.getName()) {
            case "Administracion de Cuentas":
            case "Inversiones a Plazo":
            case "Pagare Tradicional":
            case "Indicadores Finanieros":
            case "Pagos de Impuestos":
            case "Go Pay":
            case "Plataforma de Inversion":
                i.setClass(this, NoBrowser.class);
                bundle.putString(KEY_LINK, "https://www.santander.co.uk/uk/");
                break;
            case "Banca Privada":
                i.setClass(this, NoBrowser.class);
                bundle.putString(KEY_LINK, "https://www.santander.com.mx/bp/home/");
                break;
            case "Santander Plus":
                i.setClass(this, NoBrowser.class);
                bundle.putString(KEY_LINK, "https://servicios.santander.com.mx/Plus/movil.html");
                break;
        }
        if (!i.getClass().equals(null)) {
            Log.d("RRS - resolveActivity", "lanzando activity");
            i.putExtras(bundle);
            startActivity(i);
        }
        Log.d("RRS - resolveActivity", "lanzamiento finalizado");
    }

    public void alertaUnabled(final App app) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(app.getName());
        alertDialog.setMessage(app.getDescription());
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Habilitar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /*Toast.makeText(Dashboard.this, "Habilitando " + app.getName() , Toast.LENGTH_SHORT).show();*/
                        updateAppEnabled(app.getName(), "true");
                        manageApps();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void alertaSalir() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Logout");
        alertDialog.setMessage("Esta seguro que desea salir?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Salir",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void alertaApps() {
        AppsUtil appsUtil = getAppsCatalog();
        Log.d("RRS - alertaApps", appsUtil.toString());

        final CharSequence[] items = appsUtil.getAppNamesCharSequence();
        final boolean[] checkedItems = appsUtil.getAppAddedStatus();

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Selecciona Tu App")
                .setMultiChoiceItems(items, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        if (isChecked) {
                            updateAppAdded(items[indexSelected].toString(), "true");
                        } else {
                            updateAppAdded(items[indexSelected].toString(), "false");
                        }
                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        /*String selectedApps="AppsUtil selected: ";
                        for (CharSequence item:items) {
                            selectedApps = selectedApps + item.toString();
                        }
                        Toast.makeText(Dashboard.this, selectedApps, Toast.LENGTH_LONG).show();*/
                        manageApps();
                    }
                })
                /*.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel
                    }
                })*/
                .create();
        dialog.show();
    }

    private void dbQuickQuery() {
        DBHelper dbHelper = new DBHelper(this, DBHelper.DB_NAME, null, DBHelper.DB_VERSION);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(DBHelper.QUERY_TEST, null);

        String apps = "";
        while (cursor.moveToNext()) {
            Log.d("RRS", cursor.getString(0));
            apps = apps + cursor.getString(0) + "\n";
        }
        Toast.makeText(this, apps, Toast.LENGTH_LONG).show();
    }

    private App getApp(String name) {
        DBHelper dbHelper = new DBHelper(this, DBHelper.DB_NAME, null, DBHelper.DB_VERSION);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(DBHelper.QUERY_APP_BY_NAME, new String[]{name});

        App app = new App();
        while (cursor.moveToNext()) {
            String appName = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_NAME));
            String appAdded = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_ADDED));
            String appEnabled = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_ENABLED));
            String appDesc = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_DESC));

            app.setName(appName);
            boolean val = Boolean.parseBoolean(appAdded);
            app.setAdded(val);
            boolean val2 = Boolean.parseBoolean(appEnabled);
            app.setEnabled(val2);
            app.setDescription(appDesc);
        }

        return app;
    }

    private AppsUtil getAppsCatalog() {
        DBHelper dbHelper = new DBHelper(this, DBHelper.DB_NAME, null, DBHelper.DB_VERSION);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(DBHelper.QUERY_APPS_TO_ADD, null);

        AppsUtil appsUtil = new AppsUtil();
        while (cursor.moveToNext()) {
            String appName = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_NAME));
            String appAdded = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_ADDED));

            Log.d("RRS - getAppsCatalog", appName + " | " + appAdded);

            App app = new App();
            app.setName(appName);
            boolean val = Boolean.parseBoolean(appAdded);
            app.setAdded(val);

            appsUtil.getApps().add(app);
        }

        return appsUtil;
    }

    private AppsUtil getApps() {
        DBHelper dbHelper = new DBHelper(this, DBHelper.DB_NAME, null, DBHelper.DB_VERSION);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(DBHelper.QUERY_APPS_ADDED, null);

        AppsUtil appsUtil = new AppsUtil();
        while (cursor.moveToNext()) {
            String appName = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_NAME));
            String appAdded = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_ADDED));
            String appEnabled = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_ENABLED));
            String appDesc = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_DESC));

            Log.d("RRS - getApps", appName + " | " + appAdded);

            App app = new App();

            app.setName(appName);
            boolean val = Boolean.parseBoolean(appAdded);
            app.setAdded(val);
            boolean val2 = Boolean.parseBoolean(appEnabled);
            app.setEnabled(val2);
            app.setDescription(appDesc);

            appsUtil.getApps().add(app);
        }

        return appsUtil;
    }

    /*private CharSequence[] getNotAddedApps() {
        DBHelper dbHelper = new DBHelper(this, DBHelper.DB_NAME, null, DBHelper.DB_VERSION);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(DBHelper.QUERY_APPS_TO_ADD,null);

        List<String> items = new ArrayList<String>();

        while (cursor.moveToNext()){
            Log.d("RRS", cursor.getString(0));
            items.add(cursor.getString(0));
        }

        return items.toArray(new CharSequence[items.size()]);
    }*/

    public boolean updateApp(String id, String name, String added, String enabled, String description) {
        DBHelper dbHelper = new DBHelper(this, DBHelper.DB_NAME, null, DBHelper.DB_VERSION);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DBHelper.KEY_ROWID, id);
        cv.put(DBHelper.KEY_NAME, name);
        cv.put(DBHelper.KEY_ADDED, added);
        cv.put(DBHelper.KEY_ENABLED, enabled);
        cv.put(DBHelper.KEY_DESC, description);

        int result = db.update(DBHelper.TABLE_NAME, cv, "_id = ?", new String[]{id});
        return (result == 1) ? true : false;
    }

    public boolean updateAppAdded(String name, String isAdded) {
        DBHelper dbHelper = new DBHelper(this, DBHelper.DB_NAME, null, DBHelper.DB_VERSION);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        /*cv.put(DBHelper.KEY_NAME,name);*/
        cv.put(DBHelper.KEY_ADDED, isAdded);

        int result = db.update(DBHelper.TABLE_NAME, cv, "nombre = ?", new String[]{name});
        return (result == 1) ? true : false;
    }

    public boolean updateAppEnabled(String name, String isEnabled) {
        DBHelper dbHelper = new DBHelper(this, DBHelper.DB_NAME, null, DBHelper.DB_VERSION);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        /*cv.put(DBHelper.KEY_NAME,name);*/
        cv.put(DBHelper.KEY_ENABLED, isEnabled);

        int result = db.update(DBHelper.TABLE_NAME, cv, "nombre = ?", new String[]{name});
        return (result == 1) ? true : false;
    }

    public void resetAction(MenuItem item) {
        String msg = resetAppStatus() ? "Exito" : "Hubo un problema al quitar apps";
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        manageApps();
    }

    public boolean resetAppStatus() {
        DBHelper dbHelper = new DBHelper(this, DBHelper.DB_NAME, null, DBHelper.DB_VERSION);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DBHelper.KEY_ADDED,"false");
        cv.put(DBHelper.KEY_ENABLED, "false");

        int result = db.update(DBHelper.TABLE_NAME, cv, "_id > ?", new String[]{"0"});
        return (result > 0) ? true : false;
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((Dashboard) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
