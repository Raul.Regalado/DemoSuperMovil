package com.santa.demosupermovil;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    private final String CLA = "RRS DBHelper";

    // Main table
    public static final String DB_NAME = "SantanderDB";
    public static final String TABLE_NAME = "Apps";
    /*public static final String DB_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";*/
    public static final String KEY_ROWID = "_id";
    public static final String KEY_NAME = "nombre";
    public static final String KEY_ADDED = "agregada";
    public static final String KEY_ENABLED =  "habilitada";
    public static final String KEY_DESC =  "descripcion";
    public static final int DB_VERSION = 6;

    // QUERIES

    public static final String QUERY_TEST = "select nombre from AppsUtil";
    public static final String QUERY_APPS_TO_ADD = "select nombre, agregada from apps;";
    public static final String QUERY_APPS_ADDED = "select nombre, agregada, habilitada, descripcion from apps where agregada = 'true';";
    public static final String QUERY_APP_BY_NAME = "select nombre, agregada, habilitada, descripcion from apps where nombre = ?;";

    private final String INSERT1 = "insert into Apps(nombre, agregada, habilitada, descripcion) values('Administracion de Cuentas','false','false','Administracion de Cuentas: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex magni obcaecati, quibusdam saepe tempora voluptatibus.');";
    private final String INSERT2 = "insert into Apps(nombre, agregada, habilitada, descripcion) values('Inversiones a Plazo','false','false','Inversiones a Plazo: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex magni obcaecati, quibusdam saepe tempora voluptatibus.');";
    private final String INSERT3 = "insert into Apps(nombre, agregada, habilitada, descripcion) values('Pagare Tradicional','false','false','Pagare Tradicional: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex magni obcaecati, quibusdam saepe tempora voluptatibus.');";
    private final String INSERT4 = "insert into Apps(nombre, agregada, habilitada, descripcion) values('Indicadores Finanieros','false','false','Indicadores Finanieros: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex magni obcaecati, quibusdam saepe tempora voluptatibus.');";
    private final String INSERT5 = "insert into Apps(nombre, agregada, habilitada, descripcion) values('Pagos de Impuestos','false','false','Pagos de Impuestos: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex magni obcaecati, quibusdam saepe tempora voluptatibus.');";
    private final String INSERT6 = "insert into Apps(nombre, agregada, habilitada, descripcion) values('Banca Privada','false','false','Banca Privada: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex magni obcaecati, quibusdam saepe tempora voluptatibus.');";
    private final String INSERT7 = "insert into Apps(nombre, agregada, habilitada, descripcion) values('Go Pay','false','false','Go Pay: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex magni obcaecati, quibusdam saepe tempora voluptatibus.');";
    private final String INSERT8 = "insert into Apps(nombre, agregada, habilitada, descripcion) values('Plataforma de Inversion','false','false','Plataforma de Inversion: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex magni obcaecati, quibusdam saepe tempora voluptatibus.');";
    private final String INSERT9 = "insert into Apps(nombre, agregada, habilitada, descripcion) values('Santander Plus','false','false','Santander Plus: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex magni obcaecati, quibusdam saepe tempora voluptatibus.');";



    private final String
            CREATE_TABLE = "CREATE TABLE if not exists " + TABLE_NAME + " (" +
            KEY_ROWID + " integer PRIMARY KEY autoincrement," +
            KEY_NAME + " TEXT(50)," +
            KEY_ADDED + " TEXT(10)," +
            KEY_ENABLED + " TEXT(10)," +
            KEY_DESC + " TEXT(200)" +
            ");";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        Log.d(CLA,"DBHelper instantiated");
    }

    // ## Extended methods implementation ## //

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(CLA, "onCreate");

        db.execSQL(CREATE_TABLE);
        Log.d(CLA, "DB Created");

        try {
            db.execSQL(INSERT1);
            db.execSQL(INSERT2);
            db.execSQL(INSERT3);
            db.execSQL(INSERT4);
            db.execSQL(INSERT5);
            db.execSQL(INSERT6);
            db.execSQL(INSERT7);
            db.execSQL(INSERT8);
            db.execSQL(INSERT9);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(CLA, "onUpgrade");

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
