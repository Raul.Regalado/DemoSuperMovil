package com.santa.demosupermovil;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;


public class NoBrowser extends Activity {

    private String url = "https://www.santander.com/csgs/Satellite?appID=santander.wc.CFWCSancomQP01&c=Page&canal=CSCORP&cid=1278677303232&empr=CFWCSancomQP01&leng=en_GB&pagename=CFWCSancomQP01%2FPage%2FCFQP01_PageGaleriaVideos_PT21B";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        url = bundle.getString(Dashboard.KEY_LINK,url);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_no_browser);
    }

    @Override
    protected void onResume() {
        super.onResume();

        WebView webView = findViewById(R.id.web_view);
        webView.loadUrl(url);
    }
}
